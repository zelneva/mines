package dev.android.mines.model

import kotlin.math.round

class GameModel(var size: Size, level: Int) {

    var game: Game? = null
    var configuration = Configuration(size, level)


    class Configuration(var size: Size, level: Int) {
        var bombCount: Int = 0

        init {
            this.bombCount = round((size.width * size.width * level / 100).toDouble()).toInt()
        }
    }

    fun revealCellAt(point: Point) {
        if (game == null) {
            game = Game(configuration.size, point, configuration.bombCount)
        }
        game?.revealCellAt(point)
    }

    fun restart() {
        this.game = null
    }
}

