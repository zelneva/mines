package dev.android.mines.model


class Field(size: Size) {

    var size: Size = size
    var generator: Generator? = null
    var cells: Array<Cell>? = null

    init {
        this.generator = Generator(size.width)
        val range: IntRange = 0 until this.size.area
        var cells = range.map { i -> generator!!.generate(i) }
        this.cells = cells.toTypedArray()
    }

    fun get(point: Point): Cell? {
        if (isAvailable(point)) {
            return cells!![generator!!.indexForPoint(point)]
        } else return null
    }

    fun set(point: Point) {
        if (isAvailable(point)) {
            var index = generator!!.indexForPoint(point)

            cells = cells!!.drop(index).toTypedArray()
        }
    }

    fun generateLabels() {
        val minedCells = cells!!.filter { Cell.isBomb(it) }

        var bombsNeighbors = (minedCells
                .flatMap { it -> neighborsCell(it) }
                .filter { Cell.isNotBomb(it!!) }).toSet()

        for (bombNeighbors in bombsNeighbors) {
//            var label = neighborsCell(bombNeighbors!!)
//                    .fold(0) { acc, cell ->
//                        if (cell!!.type != Cell.Type.BOMB) {
//                            acc + 1
//                        } else acc
//                    }
            var label = neighborsCell(bombNeighbors!!)
            var l = 0
            for (cell in label) {
                if (cell!!.type == Cell.Type.BOMB) {
                    l++
                }

            }
            this.get(bombNeighbors!!.location)!!.label = l
            this.get(bombNeighbors!!.location)!!.type = Cell.Type.LABEL


        }
    }


    fun neighborsCell(cell: Cell): List<Cell?> {
        val points = arrayOf<Point>(
                Point(-1, -1),
                Point(-1, 0),
                Point(-1, 1),
                Point(0, -1),
                Point(0, 1),
                Point(1, -1),
                Point(1, 0),
                Point(1, 1))

        return points
                .map { it + cell.location }
                .filter { isAvailable(it) }
                .map { this.get(it) }
        //flatMap не работает
    }

    fun isAvailable(point: Point): Boolean {
        return (0 until size.height).contains(point.y)
                && (0 until size.width).contains(point.x)

    }

    fun putLabel() {
        for (cell in this!!.cells!!) {
            if (cell.type == Cell.Type.BOMB) {
                for (i in -1..1) {
                    for (j in -1..1) {
                        val newX = cell.location.x + i
                        val newY = cell.location.y + j
                        if (this!!.isAvailable(Point(newX, newY)) && Cell.isNotBomb(this!!.get(Point(newX, newY))!!)) {
                            val index = this!!.generator!!.indexForPoint(Point(newX, newY))
                            this!!.cells!![index].label++
                            this!!.cells!![index].type = Cell.Type.LABEL
                        }
                    }
                }
            }
        }
    }
}