package dev.android.mines.model

import dev.android.mines.CellType
import java.util.*

class Game(size: Size, var initialLocation: Point, var bombsCount: Int) {
    var field: Field? = null
    var openedPoints = mutableSetOf<Point>()

    init {
        this.field = Field(size)
        putBombs()
        field!!.generateLabels()
    }


    private fun putBombs() {
        for (location in generateBombsLocation()) {
            val index = field!!.generator!!.indexForPoint(location)
            field!!.get(location)?.type = Cell.Type.BOMB
        }
    }


    private fun generateBombsLocation(): Array<Point> {
        var c = field!!.get(initialLocation)
        val index = field!!.generator!!. indexForPoint(initialLocation)
        field!!.cells = field!!.cells!!.drop(index).toTypedArray()


        return (0 until  bombsCount).map { field!!.cells!!.pickRandomElement()?.location!! }.toTypedArray()
//        val index = field!!.generator!!.indexForPoint(initialLocation)
//        field!!.cells = field!!.cells!!.drop(index).toTypedArray()
//
//        val bombLocation = (0 until bombsCount)
//                .map { field!!.cells!!.pickRandomElement()?.location!! }
//
//        return ArrayList(bombLocation)
    }


    fun revealCellAt(point: Point) {
        openedPoints.add(point)
        revealNeighbors(point)
    }

    private fun revealNeighbors(point: Point){
        val cell = field?.get(point) ?: return
        if(cell.type != Cell.Type.EMPTY) return

        val neighbors = field!!.neighborsCell(cell).filter { it?.type != Cell.Type.BOMB }

        for (neighbor in neighbors){
            if (!openedPoints.contains(neighbor!!.location)){
                openedPoints.add(neighbor.location)
                if(neighbor.type == Cell.Type.EMPTY){
                    revealNeighbors(neighbor.location)
                }
            }
        }

    }
}

private fun <E> Array<E>.pickRandomElement(): E? {
    if (this.isEmpty()) return null

    val random = Random()
    val indexBomb = if (lastIndex > 0) random.nextInt(lastIndex) else 0
    val element = this[indexBomb]
    drop(indexBomb)
    return element
}

