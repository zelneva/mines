package dev.android.mines.model

data class Point(val x: Int, val y:Int){
    operator fun plus(newPoint: Point) = Point(this.x +newPoint.x, this.y + newPoint.y)
}