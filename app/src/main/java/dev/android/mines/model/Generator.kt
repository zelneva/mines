package dev.android.mines.model

class Generator(val width: Int) {

    fun generate(index: Int): Cell {
        val point = pointForIndex(index)
        return Cell(point)
    }

    fun pointForIndex(index: Int): Point {
        val x = index / this.width
        val y = index % this.width
        return Point(x, y)
    }

    fun indexForPoint(point: Point): Int {
        return point.x * this.width + point.y
    }

}
