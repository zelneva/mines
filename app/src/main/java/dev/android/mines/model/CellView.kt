package dev.android.mines.model

import android.content.Context
import android.widget.TextView
import dev.android.mines.CellType
import dev.android.mines.presenter.GamePresenter
import android.R.string.cancel
import android.app.AlertDialog
import android.content.DialogInterface
import dev.android.mines.activity.GameActivity


class CellView(var point: Point, context: Context) : TextView(context) {

    var cellType: CellType = CellType.CLOSED
        set(value) {
            field = value
            text = when (value) {
                CellType.CLOSED -> closedStr
                CellType.EMPTY -> " "
                CellType.FLAGGED -> flagStr
                CellType.BOMB -> bombStr
                CellType.LABEL -> GameActivity.gamePresenter.gameModel?.game?.field?.get(point)?.label.toString()
            }

            if(value==CellType.BOMB){
                GameActivity.showAlert(context)
            }
        }


    private val bombEmoji = 0x1F4A3
    private val flagEmogi = 0x1F6A9
    private val closedEmoji = 0x1F512

    private val bombStr = getEmojiByUnicode(bombEmoji)
    private val flagStr = getEmojiByUnicode(flagEmogi)
    private val closedStr = getEmojiByUnicode(closedEmoji)


    private fun getEmojiByUnicode(unicode: Int): String {
        return String(Character.toChars(unicode))
    }


}