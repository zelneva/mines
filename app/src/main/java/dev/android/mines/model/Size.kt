package dev.android.mines.model

data class Size(val width: Int, val height: Int) {
    val area by lazy { width * height }
}