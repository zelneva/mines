package dev.android.mines.model

data class Cell(val location: Point, var type: Type = Type.EMPTY, var label: Int = 0) {
    enum class Type {
        EMPTY,
        LABEL, //Int
        BOMB
    }

    companion object {
        fun isBomb(cell: Cell): Boolean {
            return cell.type == Type.BOMB
        }

        fun isNotBomb(cell: Cell): Boolean {
            return cell.type != Type.BOMB
        }
    }
}