package dev.android.mines.activity

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.GridView
import android.widget.TextView
import com.otaliastudios.zoom.ZoomLayout
import com.otaliastudios.zoom.ZoomLogger
import dev.android.mines.GameFieldDelegate
import dev.android.mines.GameFieldViewDataSource
import dev.android.mines.R
import dev.android.mines.adapter.GridAdapter
import dev.android.mines.model.CellView
import dev.android.mines.model.Point
import dev.android.mines.presenter.GamePresenter


class GameActivity : AppCompatActivity() {

    private lateinit var gridView: GridView
    private lateinit var newGameButton: Button

    private lateinit var countBomb: TextView
    var delegate: GameFieldDelegate? = null
    var dataSourse: GameFieldViewDataSource? = null

    var columns: Int? = null
    var level: Int? = null
    var cellViews: ArrayList<CellView>? = null

    val key = "PRESENTER"


    override fun onCreate(savedInstanceState: Bundle?) {
        ZoomLogger.setLogLevel(ZoomLogger.LEVEL_INFO)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)
//        var zoomLayout: ZoomLayout = findViewById(R.id.zoom)
//
//        zoomLayout.visibility = View.VISIBLE

//        if (savedInstanceState != null) {
//            gamePresenter = savedInstanceState.getParcelable(key)
//        } else {
        gamePresenter = GamePresenter()
//        }


        columns = intent.getIntExtra("SIZE", 0)
        level = intent.getIntExtra("LEVEL", 0)
        countBomb = findViewById(R.id.countBomb)

        createField(columns!!, columns!!)

        gamePresenter.attachView(this)

        cellViews = ArrayList(columns!! * columns!!)

        for (i in 0 until columns!!) {
            for (j in 0 until columns!!) {
                cellViews!!.add(CellView(Point(i, j), this))
            }
        }

        reloadData()

        countBomb.text = gamePresenter.gameModel!!.configuration.bombCount.toString()

        gridView.setOnItemClickListener { _, view, i, l ->
            delegate?.didSelectCellAt(cellViews!![i].point)
            adapter.notifyDataSetChanged()
        }

        newGameButton = findViewById(R.id.newGameBtn)
        newGameButton.setOnClickListener {
            gamePresenter.startNewGame()
            adapter.notifyDataSetChanged()
        }
    }


    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        if (gamePresenter != null) {
            outState?.putParcelable(key, gamePresenter)
        }
    }

    private fun createField(columns: Int, lines: Int) {
        gridView = findViewById(R.id.grid)
        gridView.numColumns = columns

            var param: ViewGroup.LayoutParams = gridView.layoutParams
            param.width = ViewGroup.LayoutParams.MATCH_PARENT
            param.height = ViewGroup.LayoutParams.MATCH_PARENT


        gridView.post {
            val width = gridView.measuredWidth
            val height = gridView.measuredHeight

            val oneWidth = width / columns
            val oneHeight = height / lines

            val oneSize = if (oneWidth > oneHeight) oneHeight else oneWidth

            val par = gridView.layoutParams
            par.width = oneSize * columns
            par.height = oneSize * lines

            adapter = GridAdapter(this, cellViews!!, oneSize)
            gridView.adapter = adapter

        }
    }


    fun reloadData() {
        val dataSource = dataSourse ?: return

        for (cell in cellViews!!) {
            cell.cellType = dataSource.cellTypeAtIndex(cell.point)
        }
    }


    companion object {
        lateinit var gamePresenter: GamePresenter
        lateinit var adapter: GridAdapter

        fun showAlert(context: Context) {
            val builder = AlertDialog.Builder(context)
            builder
                    .setMessage("Вы проиграли!")
                    .setIcon(R.drawable.bomb)
                    .setCancelable(false)
                    .setNegativeButton("Новая игра"
                    ) { dialog, id ->
                        gamePresenter.startNewGame()
                        adapter.notifyDataSetChanged()
                    }
            val alert = builder.create()
            alert.show()
        }
    }

}
