package dev.android.mines.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageButton
import dev.android.mines.R
import org.jetbrains.anko.toast

class SelectLevelActivity : AppCompatActivity() {

    private lateinit var hardButton: ImageButton
    private lateinit var middleButton: ImageButton
    private lateinit var lightButton: ImageButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_level)
        init()
    }

    private fun init() {
        hardButton = findViewById(R.id.hard_btn)
        middleButton = findViewById(R.id.middle_btn)
        lightButton = findViewById(R.id.light_btn)



        hardButton.setOnClickListener {
           startIntent(50)
        }

        middleButton.setOnClickListener {
            startIntent(25)
        }

        lightButton.setOnClickListener {
            startIntent(10)
        }
    }

    private fun startIntent(percentBomb: Int) {
        val LEVEL = "LEVEL"

        val intent = Intent(this, SelectSizeActivity::class.java)
        intent.putExtra(LEVEL, percentBomb)
        startActivity(intent)
    }
}
