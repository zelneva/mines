package dev.android.mines.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.content.Intent


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intent = Intent(this, SelectLevelActivity::class.java)
        startActivity(intent)
        finish()
    }
}