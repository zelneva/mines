package dev.android.mines.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageButton
import dev.android.mines.R

class SelectSizeActivity : AppCompatActivity() {

    private lateinit var sixButton: ImageButton
    private lateinit var sevenButton: ImageButton
    private lateinit var eightButton: ImageButton

    var level: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_size)

        level = intent.getIntExtra("LEVEL", 0)

        init()
    }

    fun init() {
        sixButton = findViewById(R.id.six_btn)
        sevenButton = findViewById(R.id.seven_btn)
        eightButton = findViewById(R.id.eight_btn)

        sixButton.setOnClickListener {
            startIntent(6)
        }

        sevenButton.setOnClickListener {
            startIntent(7)
        }

        eightButton.setOnClickListener {
            startIntent(8)
        }
    }

    private fun startIntent(size: Int) {
        val SIZE = "SIZE"
        val LEVEL = "LEVEL"
        val intent = Intent(this, GameActivity::class.java)
        intent.putExtra(SIZE, size)
        intent.putExtra(LEVEL, level)
        startActivity(intent)
    }
}