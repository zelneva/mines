package dev.android.mines

import dev.android.mines.model.Point
import dev.android.mines.model.Size

enum class CellType{
    CLOSED,
    FLAGGED,
    BOMB,
    EMPTY,
    LABEL
}

interface GameFieldViewDataSource {

    var size: Size
        get

    fun cellTypeAtIndex(point: Point): CellType


}

interface GameFieldDelegate{

    fun didSelectCellAt(point: Point)
}