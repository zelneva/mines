package dev.android.mines.presenter

import android.os.Parcel
import android.os.Parcelable
import dev.android.mines.CellType
import dev.android.mines.GameFieldDelegate
import dev.android.mines.GameFieldViewDataSource
import dev.android.mines.activity.GameActivity
import dev.android.mines.model.Cell
import dev.android.mines.model.GameModel
import dev.android.mines.model.Point
import dev.android.mines.model.Size

class GamePresenter() : GameFieldViewDataSource, GameFieldDelegate, Parcelable {

    override lateinit var size: Size

    var gameView: GameActivity? = null
    var gameModel: GameModel? = null

    constructor(parcel: Parcel) : this() {

    }


    fun attachView(gameView: GameActivity) {
        this.gameView = gameView
        this.gameView!!.delegate = this
        this.gameView!!.dataSourse = this

        val sizeField = this.gameView!!.columns
        val level = this.gameView!!.level

        gameModel = GameModel(Size(sizeField!!, sizeField), level!!)
        this.size = gameModel?.configuration!!.size
    }

    fun startNewGame() {
        gameModel!!.restart()
        gameView!!.reloadData()
    }

    override fun cellTypeAtIndex(point: Point): CellType {
        var game = gameModel?.game ?: return CellType.CLOSED
        val cell = game.field?.get(point) ?: return CellType.CLOSED

        if (game.openedPoints.contains(point)) {
            if (cell.type == Cell.Type.BOMB) return CellType.BOMB
            else if (cell.type == Cell.Type.EMPTY) return CellType.EMPTY
            else return CellType.LABEL
        }else return CellType.CLOSED
    }

    override fun didSelectCellAt(point: Point) {
        gameModel!!.revealCellAt(point)
        gameView!!.reloadData()
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {

    }

    override fun describeContents(): Int {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GamePresenter> {
        override fun createFromParcel(parcel: Parcel): GamePresenter {
            return GamePresenter(parcel)
        }

        override fun newArray(size: Int): Array<GamePresenter?> {
            return arrayOfNulls(size)
        }
    }
}
