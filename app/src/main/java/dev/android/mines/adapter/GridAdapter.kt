package dev.android.mines.adapter

import android.content.Context
import android.graphics.Color
import android.view.*
import android.widget.BaseAdapter
import android.widget.TextView
import dev.android.mines.R
import dev.android.mines.model.CellView
import org.jetbrains.anko.textColor


class GridAdapter(var context: Context, var cells: ArrayList<CellView>, var size: Int) : BaseAdapter() {


    override fun getView(position: Int, v: View?, parent: ViewGroup?): View {

        var cell = cells[position]
        var textView: TextView = TextView(context)

        var param = ViewGroup.LayoutParams(size, size)
        textView.layoutParams = param

        textView.background = context.resources.getDrawable(R.drawable.recttext)
        textView.text = cell.text
        textView.gravity = Gravity.CENTER
        textView.textSize = 27F
        textView.textColor = Color.BLACK

        return textView
    }

    override fun getItem(p0: Int): Any = cells[p0]

    override fun getItemId(p0: Int): Long = 0


    override fun getCount(): Int = cells.size

}